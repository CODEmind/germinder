# Germinder

A simple mobile App which reminds a User to wash their hands after a significant location change has been detected, as such almost certainly involves contact with contaminated surfaces.

#CodeVsCOVID19 Hackathon Project

# Description / Idea
The `Germinder` App *reminds* the User about possible contact with *germs* and informs that **they now should wash their hands** – it's a Germ Reminder.

To infer the possible contact with a contaminated surface the implementation makes use of the observation that every-day routine actions like "bring out the trash", "shop groceries", "empty the mail box", etc. always involve contact with countless surfaces: Door Handles, Lift/Elevator Panels & Buttons, Keys, Hand Rails, the Mail itself (!), PIN Panels, etc. pp. Such actions also always are accompanied by a significant change of location by the User – basically as soon as the User leaves their apartment (considered a "safe area") we can assume contact with shared and therefore possibly contaminated surfaces.

When a significant location change is detected the App notifies the User to be mindful of touched surfaces, and to be aware of not touching their face while en route.

After the User has reached their destination (no more significant location change) the App reminds the User to desinfect/wash their hands.

# Solution
The App makes use of the following technologies:
*  iOS App, for iPhone
*  CoreLocation Framework – for detection of horizontal (geographic) significant location changes, using GPS, when outdoors (walking, driving, etc.)
*  CoreMotion Framework – for detection of significant changes in altitude (vertical), using Barometer sensor, when indoors (buildings, stairwells, lift/elevator, etc.)

The first iteration will focus on changes in altitude, for the following reasons:
*  Barometer sensor works well inside buildings, whereas GPS does not.
*  Apartment & Office buildings usually have multiple stories/levels; leaving/entering the building in general involves some change in altitude.
*  Covering Apartment & Office buildings is especially important as these are places where many people come in contact with shared facilities, which makes it easy for the virus to spread.
*  Acquiring GPS Data drains the battery of the mobile device, and Users are highly sensitive about privacy when it comes to GPS/location tracking. Such concerns do not apply for the Barometer sensor as it only reports relative changes in altitude.
