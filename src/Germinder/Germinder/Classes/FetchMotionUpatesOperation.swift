//
//  FetchMotionUpatesOperation.swift
//  Germinder
//
//  Created by Dave Schöttl on 30.03.20.
//  Copyright © 2020 CODEmind Software. All rights reserved.
//

import UIKit
import os

/**

*/
class FetchMotionUpatesOperation: Operation {
	private var requestRunning = false
	private var result: MotionAnalysisResult? = nil
	
	override var isAsynchronous: Bool {
		return true
	}
	
	override var isExecuting: Bool {
		return requestRunning
	}
	
	override var isFinished: Bool {
		return result != nil
	}
	
	override func cancel() {
		super.cancel()
		
		Watchdog.sharedWatchdog.cancelAnalysisRequest()
	}
	
	func finish(result: MotionAnalysisResult) {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		guard requestRunning else { return }
		
		willChangeValue(forKey: #keyPath(isExecuting))
		willChangeValue(forKey: #keyPath(isFinished))
		
		requestRunning = false
		self.result = result
		
		didChangeValue(forKey: #keyPath(isFinished))
		didChangeValue(forKey: #keyPath(isExecuting))
		
		if result == MotionAnalysisResult.kNULLMotionAnalysisResult {
			os_log("WARNING: Invalid Result received!")
		}
		else {
			os_log("SUCCESS: Result received!")
		}
		
		//self.completionBlock!()
	}
	
	override func start() {
		willChangeValue(forKey: #keyPath(isExecuting))
		requestRunning = true
		didChangeValue(forKey: #keyPath(isExecuting))
		
		guard !isCancelled else {
			finish(result: MotionAnalysisResult.kNULLMotionAnalysisResult)
			return
		}
		
		main()
	}
	
	override func main() {
		os_log("Pinging Watchdog to request Motion Analysis...")
		
		// sleep for 2sec to allow for processing of old/buffered points
		//Thread.sleep(forTimeInterval: 2)
		
		// request fresh Analysis
		Watchdog.sharedWatchdog.requestAnalysisResultWithCompletionHandler { /*[unowned self]*/ (result: MotionAnalysisResult) in
			self.finish(result: result)
		}
	}
}
