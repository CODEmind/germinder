//
//  MotionDataProvider.swift
//  Germinder
//
//  Created by Dave Schöttl on 28.03.20.
//  Copyright © 2020 CODEmind Software. All rights reserved.
//

import CoreMotion
import os

typealias AltitudeUpdatedHandler = (_ data: CMAltitudeData, _ log: String, _ error: Error?) -> Void
typealias SignificantLocationChangeHandler = (_ result: MotionAnalysisResult) -> Void
typealias AnalysisCompletedHandler = (_ result: MotionAnalysisResult) -> Void

/**
Continuously monitors Altitude data and Pedestrian Actitiy on a background thread,
and analyzes it once enough data points are collected.

Register handler blocks to be informed about the following events:
 * Altitude Updates
 * Significant Location Change
*/
class MotionDataProvider: NSObject {
	// MARK: - IVARs
	public static let sharedMotionDataProvider: MotionDataProvider = MotionDataProvider.init()
	
	//private let pedometer: CMPedometer = CMPedometer.init()
	private let altimeter: CMAltimeter = CMAltimeter.init()
	private let analysisAlgorithm: MotionAnalysisAlgorithm = DistanceDeltaAlgorithm.init()
	
	/* --- Managed on Main Queue --- */
	private var altitudeDataBuffer: Array<CMAltitudeData> = []
	//private(set) var pedestrianActivity: CMPedometerEventType = .pause
	
	private let altitudeUpdatedHandlers: NSMutableSet = NSMutableSet.init(capacity: 0)
	private let significantLocationChangeHandlers: NSMutableSet = NSMutableSet.init(capacity: 0)
	private let analysisCompletedHandlers: NSMutableSet = NSMutableSet.init(capacity: 0)
	
	private let backgroundQueue: OperationQueue = OperationQueue.init()
	
	
	// MARK: - INIT
	override init() {
		// serialize queue, because it makes sense
		backgroundQueue.maxConcurrentOperationCount = 1
		
		super.init()
	}
	
	
	// MARK: - HANDLER REGISTRATION
	public func registerAltitudeUpdatedHandler(_ handler: @escaping AltitudeUpdatedHandler) {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		OperationQueue.main.addOperation {
		//backgroundQueue.addOperation {
			self.altitudeUpdatedHandlers.add(handler)
		//}
		}
	}
	
	public func registerSignificantLocationChangeHandler(_ handler: @escaping SignificantLocationChangeHandler) {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		OperationQueue.main.addOperation {
		//backgroundQueue.addOperation {
			self.significantLocationChangeHandlers.add(handler)
		//}
		}
	}
	
	private func registerAnalysisCompletedHandler(_ handler: @escaping AnalysisCompletedHandler) {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		OperationQueue.main.addOperation {
		//backgroundQueue.addOperation {
			self.analysisCompletedHandlers.add(handler)
		//}
		}
	}
	
	
	// MARK: - MONITORING & ANALYSIS
	public func startMonitoring() -> Bool {
		var success = false
		
		/*
		Request motion updates for detecting level change events:
		* Using CMPedometer is not suitable, as Level Change event does not
		  include Lift/Elevator movement - only walking counts.
		* Must therefore use CMAltimeter with own implementation; floor height
		  is ~3m (Apple Documentation, https://developer.apple.com/documentation/coremotion/cmpedometerdata/1613961-floorsascended)
		*/
		
		//  gradually evaluate authorization & availability
		success = (CMAltimeter.authorizationStatus() == .authorized || CMAltimeter.authorizationStatus() == .notDetermined) && CMAltimeter.isRelativeAltitudeAvailable()
		guard success else {
			os_log("ERROR: Altitude not available!")
			return false
		}
		
		/*success = (CMPedometer.authorizationStatus() == .authorized || CMPedometer.authorizationStatus() == .notDetermined) && CMPedometer.isPedometerEventTrackingAvailable()
		guard success else {
			os_log("ERROR: Pedometer Event Tracking not available!")
			return false
		}*/
		
		// start Altitude updates
		//let backgroundQueue: OperationQueue = OperationQueue.init()
		
		altimeter.startRelativeAltitudeUpdates(to: backgroundQueue /*OperationQueue.main*/) { /*[unowned self]*/ (data: CMAltitudeData?, error: Error?) in
			OperationQueue.main.addOperation {
				self.handleAltitudeUpdate(data, error: error)
			}
		}
		
		// start Pedestrian Activity Event tracking
		/*pedometer.startEventUpdates { /*[unowned self]*/ (event: CMPedometerEvent?, error: Error?) in
			guard let event = event else {
				return
			}
			
			let activity = event.type
			
			let log = ("Pedestrian Activity reported: \(activity == .pause ? "Pause" : "Resume")")
			os_log("%{public}s", log)
			
			self.pedestrianActivity = activity
		}*/
		
		return success
	}
	
	public func stopMonitoring() {
		// stop Altimeter updates
		altimeter.stopRelativeAltitudeUpdates()
		
		// stop Pedestrian Activity Event tracking
		//pedometer.stopEventUpdates()
		
		// cancel any pending Analysis Requests
		cancelAnalysisRequests()
	}
	
	public func performAnalysisWithCompletionHandler(_ handler: @escaping AnalysisCompletedHandler) {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		// store completion handler in IVAR for later invocation by Altitude Update handler
		OperationQueue.main.addOperation {
			self.registerAnalysisCompletedHandler(handler)
		}
	}
	
	public func cancelAnalysisRequests() {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		OperationQueue.main.addOperation {
			//backgroundQueue.addOperation {
			self.serveAnalysisRequests(with: MotionAnalysisResult.kNULLMotionAnalysisResult)
			//}
		}
	}
}

// MARK: - PRIVATE
private extension MotionDataProvider {
	/* --- Managed on Background Queue --- */
	func handleAltitudeUpdate(_ data: CMAltitudeData?, error: Error?) {
		guard let data = data else {
			return
		}
		
		// store data in buffer
		if altitudeDataBuffer.count == Self.kAltitudeBufferMaxCount {
			// max. count is reached: discard oldest data point (index 0)
			altitudeDataBuffer.removeFirst()
		}
		altitudeDataBuffer.append(data)
		
		// log info to console
		let relativeAltitude = data.relativeAltitude
		let pressure = data.pressure
		let timestamp = Date.init()
		
		let log = (#function + " - Date: \(timestamp) – Relative Altitude: \(relativeAltitude)m, Pressure: \(pressure)kPa" + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		os_log("%{public}s", log)
		
		// report altitude update
		for element in altitudeUpdatedHandlers {
			let handler = element as! AltitudeUpdatedHandler
			handler(data, log, error)
		}
		
		// perform analysis if enough data points available
		if altitudeDataBuffer.count >= analysisAlgorithm.minimumBatchSize() {
			// create batch using content of buffer
			let batch = Array.init(altitudeDataBuffer)
			
			// clear buffer
			altitudeDataBuffer.removeAll(keepingCapacity: true)
			
			// run analysis
			let result = analysisAlgorithm.analyzeAltitudeData(batch)
			
			// inform about significant location change
			if result.isSignificantLocationChange {
				os_log("SIGNIFICANT LOCATION CHANGE DETECTED!")
				
				for element in significantLocationChangeHandlers {
					let handler = element as! SignificantLocationChangeHandler
					handler(result)
				}
			}
			
			// serve Analysis Result requests
			serveAnalysisRequests(with: result)
		}
	}
	
	/* --- Managed on Background Queue --- */
	func serveAnalysisRequests(with result: MotionAnalysisResult) {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		// serve Analysis Result requests
		for element in analysisCompletedHandlers {
			let handler = element as! AnalysisCompletedHandler
			handler(result)
		}
		
		// clear served request handlers
		analysisCompletedHandlers.removeAllObjects()
	}
}

// MARK: - CONSTANTS
extension MotionDataProvider {
	static let kAltitudeBufferMaxCount: Int = 180 // limit length of buffer; 180 updates =~ 180 seconds (3min)
}


/*
Request motion updates for detecting level change events:
1. Use CMPedometer if available, provides specific event for Level Change.
2. Fall-back to Altimeter and use custom implementation to detect Level Change.
3. Return 'false' if features are not supported/available.
*/

// let's see how Swift handles this - by C convention it should stop evaluation of the if the first part of the OR statement resolves to 'false'
/*success = CMPedometer.authorizationStatus() == .authorized || CMPedometer.isFloorCountingAvailable()
if success {
	levelChangedEventHandler = handler
	pedometer.startEventUpdates { [unowned self] (event: CMPedometerEvent?, error: Error?) in
		self.levelChangedEventHandler!(error)
	}
}
else {
	
}

guard success else {
	os_log("No Auth for Pedometer!")
	
	
	return false
}*/
