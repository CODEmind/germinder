//
//  MotionAnalysisAlgorithm.swift
//  Germinder
//
//  Created by Dave Schöttl on 30.03.20.
//  Copyright © 2020 CODEmind Software. All rights reserved.
//

import CoreMotion

protocol MotionAnalysisAlgorithm {
	func analyzeAltitudeData(_ data: [CMAltitudeData]) -> MotionAnalysisResult
	func minimumBatchSize() -> Int
}
