//
//  Watchdog.swift
//  Germinder
//
//  Created by Dave Schöttl on 30.03.20.
//  Copyright © 2020 CODEmind Software. All rights reserved.
//

import UIKit
import UserNotifications
import os

class Watchdog: NSObject {
	public static let sharedWatchdog: Watchdog = Watchdog.init()
	
	func requestAnalysisResultWithCompletionHandler(_ completionHandler: @escaping AnalysisCompletedHandler) {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		MotionDataProvider.sharedMotionDataProvider.performAnalysisWithCompletionHandler { [unowned self] (result: MotionAnalysisResult) in
			os_log("analysis result received")
			if result == MotionAnalysisResult.kNULLMotionAnalysisResult {
				// received Dummy result: bail-out
				os_log("WARNING: 'kNULLMotionAnalysisResult' received!")
				completionHandler(result)
				return
			}
			
			/*
			TODO:
			1) Inspect Result & record for time frame analysis
			2) Decide on appropriate Notification: WARNING (start of journey), or REMINDER (reached destination)
			3) Schedule/show UserNotification
			*/
			
			// POC: Post UserNotification 'WARNING'
			//if result.isSignificantLocationChange {
				self.postWarningNotification()
			//}
			
			// invoke completionHandler
			completionHandler(result)
		}
	}
	
	func cancelAnalysisRequest() {
		os_log(#function)
		
		MotionDataProvider.sharedMotionDataProvider.cancelAnalysisRequests()
	}
	
	func postWarningNotification() {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		
		//DispatchQueue.main.sync {
		OperationQueue.main.addOperation {
			let content = UNMutableNotificationContent.init()
			content.title = "☣️ WARNING! 🦠"
			content.subtitle = "Risk of Infection with COVID-19"
			content.body = " • Keep contact with shared surfaces (buttons, panels, handles, etc.) at a minimum!\n • DO NOT TOUCH YOUR FACE!\n • Maintain a distance of at least 2m to other persons!\n • Cough and sneeze in your arm pit!\n • Immediately desinfect/wash your hands when you reach your destination!"
			content.badge = NSNumber.init(value: 1)
			content.sound = UNNotificationSound.default
			
			/*let imageName = "notification_warning"
			guard let imageURL = Bundle.main.url(forResource: imageName, withExtension: "jpg") else {
				os_log("ERROR: Image '\(imageName).jpg' not found!")
				return
			}
			
			let attachment = try! UNNotificationAttachment.init(identifier: imageName, url: imageURL, options: .none)
			content.attachments = [attachment]*/
			
			let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
			let request = UNNotificationRequest.init(identifier: "notification.warning.id", content: content, trigger: trigger)
			
		//os_log("adding request to notification center, switch to Home Screen now!")
		
			UNUserNotificationCenter.current().add(request) { (error: Error?) in
				guard let error = error else {
					os_log("INFO - User Notification scheduled!")
					return
				}
				
				os_log("%{public}s", "ERROR - Error while adding User Notification: \(error.localizedDescription)")
			}
		}
		
	}
}
