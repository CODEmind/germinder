//
//  DistanceDeltaAlgorithm.swift
//  Germinder
//
//  Created by Dave Schöttl on 30.03.20.
//  Copyright © 2020 CODEmind Software. All rights reserved.
//

import CoreMotion
import os

// MARK: - PROTOCOL IMPLEMENTATION
class DistanceDeltaAlgorithm: NSObject, MotionAnalysisAlgorithm {
	func analyzeAltitudeData(_ data: [CMAltitudeData]) -> MotionAnalysisResult {
		// determine range
		var minValue: Double = 0.0
		var maxValue: Double = 0.0
		
		if let first = data.first {
			minValue = first.relativeAltitude.doubleValue
			maxValue = minValue
		}
		
		for altitudeData in data {
			let currentValue = altitudeData.relativeAltitude.doubleValue
			
			if currentValue <= minValue {
				minValue = currentValue
				continue
			}
			
			if currentValue >= maxValue {
				maxValue = currentValue
				continue
			}
		}
		
		// compute absolute distance
		let distance: Double
		if (minValue <= 0.0 && maxValue <= 0.0) || (minValue >= 0.0 && maxValue >= 0.0) {
			distance = fabs(fabs(maxValue) - fabs(minValue))
		}
		else {
			// min <= 0.0, max >= 0.0
			distance = fabs(minValue) + maxValue
		}
		
		// check if distance breaks threshold
		let isSignificantLocationChange = (distance >= Self.kDistanceThreshold)
		
		// os_log stats to console
		let logMessage = String.init(format: "Analysis Complete – data points processed: %lu, minValue: %.2f, maxValue: %.2f, absolute distance: %.2f, significant location change: %@",
									 data.count,
									 minValue,
									 maxValue,
									 distance,
									 (isSignificantLocationChange ? "YES" : "NO"))
		os_log("%{public}s", logMessage)
		
		// return result
		let result = MotionAnalysisResult.init(withDistance: distance,
											   isSignificantLocationChange: isSignificantLocationChange,
											   statistics: logMessage)
		return result
	}
	
	func minimumBatchSize() -> Int {
		return Self.kMinimumBatchSize
	}
}

// MARK: - CONSTANTS
extension DistanceDeltaAlgorithm {
	static let kDistanceThreshold: Double = 0.5 // Standard Story Height: ~3.0m
	static let kMinimumBatchSize: Int = 15 // ~1 update/sec; max. Runtime for Fetch is 30sec, so this should not exceed ~25sec.
}

