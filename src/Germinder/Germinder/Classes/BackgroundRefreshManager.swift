//
//  BackgroundRefreshManager.swift
//  Germinder
//
//  Created by Dave Schöttl on 29.03.20.
//  Copyright © 2020 CODEmind Software. All rights reserved.
//

import BackgroundTasks
import os

// MARK: - PUBLIC API
class BackgroundRefreshManager: NSObject {
	public static let sharedBackgroundRefreshManager = BackgroundRefreshManager.init()
	let queue = OperationQueue.init()
	
	override init() {
		super.init()
		
		// setup Queue
		queue.maxConcurrentOperationCount = 1
		queue.name = Self.kRefreshOperationQueueName
	}
	
	public func registerTasks() {
		let success = BGTaskScheduler.shared.register(forTaskWithIdentifier: Self.kRefreshTaskIdentifier, using: /*DispatchQueue.main*/ nil) { (task: BGTask) in
			os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
			os_log("%{public}s", #function + " – task was invoked! \(task)")
			
			OperationQueue.main.addOperation {
				self.handleAppRefresh(task: task as! BGAppRefreshTask)
			}
		}
		
		os_log("%{public}s", #function + " --> success: \(success ? "YES" : "NO")")
	}
	
	public func scheduleAppRefresh() {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		let request = BGAppRefreshTaskRequest.init(identifier: Self.kRefreshTaskIdentifier)
		request.earliestBeginDate = Date.init(timeIntervalSinceNow: Self.kRefreshIntervalSeconds) // refresh every n seconds
		
		do {
			try BGTaskScheduler.shared.submit(request)
		} catch {
			os_log("%{public}s", "ERROR – Unable to schedule App Refresh Task: \(error)")
		}
	}
}

// MARK: - TASK HANDLING
private extension BackgroundRefreshManager {
	func handleAppRefresh(task: BGAppRefreshTask) {
		os_log("%{public}s", #function + " - isMainThread: \(Thread.isMainThread ? "YES" : "NO")")
		
		// schedule next Task
		scheduleAppRefresh()
		
		// setup Queue
		/*let queue = OperationQueue.init()
		queue.maxConcurrentOperationCount = 1*/
		
		/*let success = MotionDataProvider.sharedMotionDataProvider.startMonitoring()
		if success {
			os_log("INFO: Motion Monitoring ACTIVE 🧐")
		}
		else {
			os_log("WARNING: Motion Monitoring DENIED! 🙄")
		}*/
		
		
		
		// create Operation which implements the logic for the Background Task
		let operation = FetchMotionUpatesOperation.init()
		/*let operation = BlockOperation.init()
		operation.addExecutionBlock {
			os_log("Pinging Watchdog to request Motion Analysis...")
			
			Watchdog.sharedWatchdog.requestAnalysisResultWithCompletionHandler { [unowned operation] (result: MotionAnalysisResult) in
				guard result != MotionAnalysisResult.kNULLMotionAnalysisResult else {
					os_log("ERROR: Cancelling Operation!")
					operation.cancel()
					operation.completionBlock!()
					return
				}
				
				os_log("SUCCESS: Invoking Completion Block!")
				operation.completionBlock!()
			}
		}*/
		
		// Task Expiration Handler cancels Operation, which in turn invokes completionBlock (below)
		task.expirationHandler = {
			os_log("%{public}s", "WARNING – Background Task '\(task.identifier)' has expired; canceling associated Operation '\(operation)'.")
			self.queue.cancelAllOperations()
		}
		
		// Operation has completed the work: Inform OS that Background Task has finished
		operation.completionBlock = {
			//MotionDataProvider.sharedMotionDataProvider.stopMonitoring()
			
			task.setTaskCompleted(success: !operation.isCancelled)
		}
		
		// Start the Operation
		queue.addOperations([operation], waitUntilFinished: false)
	}
}

// MARK: - CONSTANTS
private extension BackgroundRefreshManager {
	static let kRefreshOperationQueueName: String = "ch.codemind.Germinder.RefreshOperationQueue"
	static let kRefreshTaskIdentifier: String = "ch.codemind.Germinder.RefreshTask"
	static let kRefreshIntervalSeconds: TimeInterval = 60 // every 5 minute
}

