//
//  MotionAnalysisResult.swift
//  Germinder
//
//  Created by Dave Schöttl on 30.03.20.
//  Copyright © 2020 CODEmind Software. All rights reserved.
//

import UIKit

class MotionAnalysisResult: NSObject {
	public let timestamp: Date = Date.init()
	public let distance: Double
	public let isSignificantLocationChange: Bool
	public let statistics: String
	
	init(withDistance distance: Double, isSignificantLocationChange: Bool, statistics: String) {
		self.distance = distance
		self.isSignificantLocationChange = isSignificantLocationChange
		self.statistics = statistics
		
		super.init()
	}
}

// MARK: - CONSTANTS
extension MotionAnalysisResult {
	public static let kNULLMotionAnalysisResult: MotionAnalysisResult = MotionAnalysisResult.init(withDistance: -999.0,
																								  isSignificantLocationChange: false,
																								  statistics: "Meaningless Placeholder/NULL Object")
}
