//
//  DataLogTableViewController.swift
//  Germinder
//
//  Created by Dave Schöttl on 28.03.20.
//  Copyright © 2020 CODEmind Software. All rights reserved.
//

import UIKit
import CoreMotion
import os

class DataLogTableViewController: UITableViewController {

	var entries: Array<CMAltitudeData> = []
	var isRecording: Bool = false
	
	@IBOutlet var startStopBarButtonItem: UIBarButtonItem!
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		// self.navigationItem.rightBarButtonItem = self.editButtonItem
		
		MotionDataProvider.sharedMotionDataProvider.registerAltitudeUpdatedHandler {
			/*[unowned self]*/ (data: CMAltitudeData, log: String, error: Error?) in
			
			OperationQueue.main.addOperation {
				self.entries.append(data)
				
				self.tableView.reloadData()
				self.tableView.scrollToRow(at: IndexPath.init(row: self.entries.count - 1, section: 0), at: .bottom, animated: true)
			}
		}
		
		MotionDataProvider.sharedMotionDataProvider.registerSignificantLocationChangeHandler { (result: MotionAnalysisResult) in
			OperationQueue.main.addOperation {
				guard result != MotionAnalysisResult.kNULLMotionAnalysisResult else {
					os_log("%{public}s", "WARNING: Received Dummy MotionAnalysisResult! \(result)")
					return
				}
				
				// os_log to console
				let log = String.init(format: "INFO: Significant Location Change detected, with Vertical Delta [m]: %.2f", result.distance)
				os_log("%{public}s", log)
				
				Watchdog.sharedWatchdog.postWarningNotification()
			}
		}
	}
	
	
	@IBAction func toggleRecordingAction(_ sender: UIBarButtonItem) {
		// reset badge
		UIApplication.shared.applicationIconBadgeNumber = 0
		
		/*Watchdog.sharedWatchdog.requestAnalysisResultWithCompletionHandler { (result: MotionAnalysisResult) in
			os_log("Analysis complete: \(result)")
		}*/
		
		/*if isRecording {
			startStopBarButtonItem.title = "Start"
			MotionDataProvider.sharedMotionDataProvider.stopLevelChangeMonitoring()
			navigationItem.prompt = "Recording Stopped"
			isRecording = false
		}
		else {
			isRecording = MotionDataProvider.sharedMotionDataProvider.startLevelChangeMonitoringWithHandler(
				altitudeUpdatedHandler: {
					[unowned self] (data: CMAltitudeData, log: String, error: Error?) in
					self.entries.append(data)
					
					self.tableView.reloadData()
					self.tableView.scrollToRow(at: IndexPath.init(row: self.entries.count - 1, section: 0), at: .bottom, animated: true)
				},
				significantLocationChangeHandler: {
					[unowned self] (result: MotionAnalysisResult) in
					guard result != MotionAnalysisResult.kNULLMotionAnalysisResult else {
						os_log("WARNING: Received Dummy MotionAnalysisResult! \(result)")
						return
					}
					
					// os_log to console
					let log = String.init(format: "INFO: Significant Location Change detected, with Vertical Delta [m]: %.2f", result.distance)
					os_log(log)
					
					
					Watchdog.sharedWatchdog.postWarningNotification()
					
					// TODO: Indicate Significant Location Change somehow in UI.
			})
			
			if isRecording {
				startStopBarButtonItem.title = "Stop"
				navigationItem.prompt = "Recording Active…"
			}
		}*/
	}
	
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return entries.count
    }
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultLabelCell", for: indexPath)
		
		let data = entries[indexPath.row]
		cell.textLabel?.text = String.init(format: "Rel. Altitude [m]: %.2f", data.relativeAltitude.floatValue)
		cell.detailTextLabel?.text = String.init(format: "Pressure [kPa]: %.2f, Timestamp: %.0f", data.pressure.floatValue, data.timestamp)

        return cell
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
